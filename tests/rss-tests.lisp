(define-test rss-tests :parent all-tests)

(define-test rss-post-compile
    :parent rss-tests
    (let ((template (make-hash-table :test #'equal)))
      (setf (gethash "tpath" template) "templates/rss/main.rss")
      (setf (gethash "rss" *output*) (list nil ".rssi" template ""))
      (setf (gethash :file *args*) "tests/rss/01.lispdown")
      (rss-post-compile)
      (let ((output (with-open-file (f "tests/rss/rss.xml")
		      (loop as l = (read-line f nil) while l collecting l)))
	    (correct (with-open-file (f "tests/rss/correct-post-hook.xml")
		       (loop as l = (read-line f nil) while l collecting l))))
	(is equal output correct))
      (setf (gethash :file *args*) nil)
      (setf (gethash "rss" *output*) (list nil
					   ".rssi"
					   (make-hash-table :test #'equal)
					   ""))))

;;; Test all of the rss output functions
(define-test rss-output
    :skip
    :parent rss-tests   ;          _
    (rss-text-pre)      ; <p>       \
    (rss-text "a")      ;   a        \
    (rss-bold-pre)      ;   <strong>  \
    (rss-bold "b")      ;     b        |
    (rss-bold-post)     ;   </strong>  |-- Included in output
    (rss-slant-pre)     ;   <i>        |
    (rss-slant "c")     ;    c        /
    (rss-slant-post)    ;   </i>     /
    (rss-text-post)     ; </p> _   _/
    (rss-text-pre)      ; <p>   \
    (rss-text "d")      ;   d    |-- Not included in output
    (rss-text-post)     ; </p> _/
    (is equal "<p>a<strong>b</strong><i>c</i></p>"
	(nth 3 (gethash "rss" *output*)))
    (setf (nth 3 (gethash "rss" *output*)) ""))
