;;; Load Parachute
(ql:quickload "parachute")
(in-package :parachute)

;;; Disable style warnings (sbcl likes to clog up the repl with complaints about
;;; functions used before their definition).
(declaim (sb-ext:muffle-conditions cl:warning))

;;; Set the path to the project's top level directory and load the compiler
(setf *default-pathname-defaults* (truename ".."))
(load "./macros.lisp")
(load "./primitives.lisp")
(setf *run-main* nil)
(load "./lispdown.lisp")

;;; Reset *output* between tests
(defun reset-html-output ()
  (setf *output* (make-hash-table :test #'equal))
  (setf (gethash "html" *output*) (list nil ".html"
					(make-hash-table :test #'equal) ""))
  (setf (gethash "rss"  *output*) (list nil ".rssi"
					(make-hash-table :test #'equal) "")))

;;; Top Level Test Suite
(define-test all-tests)

;;; ==========================================================================
;;; ============================== Parser Tests ==============================
;;; ==========================================================================

(define-test parser-suite :parent all-tests)

(define-test top-level-parser
    :parent parser-suite
    (parse '(("bold" "a")
	     ("code" "tests/test-template")
	     ("image" "tests/image.png")
	     ("link" "c" "site.com")
	     ("list" "d")
	     ("section" "e"
	      ("slant" "f")
	      ("table" ("g"))
	      ("text" "h"))))
    (is equal (format nil "<strong>a </strong><pre>(a~%  (b c~%    (d e f)))~%</pre><img src=\"tests/image.png\"/><a href=\"site.com\">c</a><ul><li>d </li></ul><h1>e</h1><i>f </i><table><tbody><tr><td>g </td></tr></tbody></table><p>h </p>")
	(nth 3 (gethash "html" *output*)))
    (reset-html-output))

(define-test top-level-parser-invalid-token
    :parent parser-suite
    (fail (parse '(("invalid" "a"))))
    (reset-html-output))

;;; Test parse-bold's ability to handle a series of strings
(define-test parse-bold
    :parent parser-suite
    (parse-bold '("hello" "world!"))
    (rss-set-body)
    (is equal "<strong>hello world! </strong>" (nth 3 (gethash "html" *output*)))
    (is equal "<strong>hello world! </strong>" (nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-bold's ability to handle conses
(define-test parse-bold-cons
    :parent parser-suite
    (parse-bold '(("bold" "hello world!")))
    (rss-set-body)
    (is equal "<strong><strong>hello world! </strong></strong>"
	(nth 3 (gethash "html" *output*)))
    (is equal "<strong><strong>hello world! </strong></strong>"
	(nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-code's ability to handle existant files
(define-test parse-code
    :parent parser-suite
    (parse-code '("tests/test-template"))
    (is equal (format nil "<pre>(a~%  (b c~%    (d e f)))~%</pre>")
	(nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-code's ability to handle nonexistant files
(define-test parse-code-nonexistant
    :parent parser-suite
    (fail (parse-code '("nonexistant")))
    (reset-html-output))

;;; Test parse-image's ability to handle existant files
(define-test parse-image
    :parent parser-suite
    (parse-image '("tests/image.png"))
    (is equal "<img src=\"tests/image.png\"/>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-image's ability to handle nonexistant files
(define-test parse-image-nonexistant
    :parent parser-suite
    (fail (parse-image '("nonexistant")))
    (reset-html-output))

;;; Test parse-link
(define-test parse-link
    :parent parser-suite
    (parse-link '("Link" "www.site.com"))
    (is equal "<a href=\"www.site.com\">Link</a>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-list's ability to handle a list of strings
(define-test parse-list-strings
    :parent parser-suite
    (parse-list '("a" "b" "c"))
    (is equal "<ul><li>a </li><li>b </li><li>c </li></ul>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-list's ability to handle a list of conses
(define-test parse-list-conses
    :parent parser-suite
    (parse-list '(("bold" "a") ("bold" "b")))
    (is equal "<ul><li><strong>a </strong></li><li><strong>b </strong></li></ul>"
	(nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-section's ability to handle a single level of depth
(define-test parse-section
    :parent parser-suite
    (parse-section '("a" ("bold" "b")))
    (is equal "<h1>a</h1><strong>b </strong>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-section's ability to handle sections with depth
(define-test parse-section-nested
    :parent parser-suite
    (parse-section '("a" ("section" "b" ("section" "c" ("bold" "d")))))
    (is equal "<h1>a</h1><h2>b</h2><h3>c</h3><strong>d </strong>"
	(nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-section to make sure it fails on improper data
(define-test parse-section-invalid
    :parent parser-suite
    (fail (parse-section '("a" "b")))
    (reset-html-output))

;;; Test parse-slant's ability to handle strings
(define-test parse-slant-string
    :parent parser-suite
    (parse-slant '("a"))
    (rss-set-body)
    (is equal "<i>a </i>" (nth 3 (gethash "html" *output*)))
    (is equal "<i>a </i>" (nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-slant's ability to handle conses
(define-test parse-slant-cons
    :parent parser-suite
    (parse-slant '(("slant" "a")))
    (rss-set-body)
    (is equal "<i><i>a </i></i>" (nth 3 (gethash "html" *output*)))
    (is equal "<i><i>a </i></i>" (nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-table's ability to handle lists of strings
(define-test parse-table-strings
    :parent parser-suite
    (parse-table '(("a") ("b" "c")))
    (is equal "<table><tbody><tr><td>a </td></tr><tr><td>b </td><td>c </td></tr></tbody></table>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test parse-table's ability to handle lists of conses
(define-test parse-table-conses
    :parent parser-suite
    (parse-table '((("bold" "a"))))
    (is equal
	"<table><tbody><tr><td><strong>a </strong></td></tr></tbody></table>"
	(nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test that parse-table fails on malformed data
(define-test parse-table-fail
    :parent parser-suite
    (fail (parse-table '("not a cons")))
    (fail (parse-table '((123))))
    (reset-html-output))

;;; Test parse-text's ability to handle strings
(define-test parse-text-string
    :parent parser-suite
    (parse-text '("a"))
    (rss-set-body)
    (is equal "<p>a </p>" (nth 3 (gethash "html" *output*)))
    (is equal "<p>a </p>" (nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-text's ability to handle conses
(define-test parse-text-cons
    :parent parser-suite
    (parse-text '(("bold" "a")))
    (rss-set-body)
    (is equal "<p><strong>a </strong></p>" (nth 3 (gethash "html" *output*)))
    (is equal "<p><strong>a </strong></p>" (nth 3 (gethash "rss" *output*)))
    (reset-html-output))

;;; Test parse-template with properly formatted data
(define-test parse-template
    :parent parser-suite
    (parse-template '("html" "tests/template"
		      ("a" "b")
		      ("c" "d")))
    (let ((tab (nth 2 (gethash "html" *output*))))
      (is equal (list "tests/template" "b" "d")
	  (list (gethash "path" tab)
		(gethash "a" tab)
		(gethash "c" tab))))
    (setf (nth 2 (gethash "html" *output*)) nil))

(load "tests/html-tests.lisp")
(load "tests/primitives-tests.lisp")
(load "tests/rss-tests.lisp")
(load "tests/macro-test.lisp")

;;; Run the tests
(test 'all-tests); :report 'interactive)
