(define-test primitives :parent all-tests)

;;; Test argument parser's ability to handle various input arg orders
(define-test argument-parser
    :parent primitives
    (setf *args* (make-hash-table))
    (setf *posix-argv* '("lispdown" "file"))
    (parse-args)
    (is equal (gethash :file *args*) "file")
    
    (setf *args* (make-hash-table))
    (setf *posix-argv* '("lispdown" "file" "-e" "a" "b" "c"))
    (parse-args)
    (is equal (gethash :file *args*) "file")
    (is equal (gethash :exclude *args*) '("c" "b" "a"))

    (setf *args* (make-hash-table))
    (setf *posix-argv* '("lispdown" "-e" "html" "file"))
    (parse-args)
    (is equal (gethash :file *args*) "file")
    (is equal (gethash :exclude *args*) '("html")))

;;; Just make sure that printing help text doesn't fail
(define-test help-text
    :parent primitives
    (finish (print-help)))

;;; Test load-template's ability to handle templates
(define-test load-template
    :parent primitives
    (let ((ht (make-hash-table :test #'equal)))
      (setf (gethash "a" ht) "world")
      (setf (gethash "b" ht) "olleh")
      (is equal (load-template "tests/template" ht)
	  (format nil "hello world!~%!dlrow olleh~%{hello world!}"))))

;;; Test that load-template will fail on a nonexistant file
(define-test load-template-nonexistant
    :parent primitives
    (fail (load-template "nonexistant" (make-hash-table))))

;;; Test that new-file-name produces correct output
(define-test file-name
    :parent primitives
    (is equal (new-file-name "abc.def" ".cba") "abc.cba")
    (is equal (new-file-name "abc.def.abc" ".ghi") "abc.def.ghi"))

(define-test write-file
    :parent primitives
    (setf tplt (make-hash-table :test #'equal))
    (setf (gethash "path" tplt) "tests/template")
    (setf (gethash "a" tplt) "world")
    (setf (gethash "b" tplt) "olleh")
    (setf (gethash :file *args*) "tests/test-output.ld")
    (setf (gethash "html" *output*) (list T ".html" tplt "body"))
    (write-out-files)
    (let ((out ""))
      (with-open-file (f "tests/test-output.html")
	(do ((line (read-line f) (read-line f nil 'eof)))
	    ((eq line 'eof) nil)
	  (setf out (concatenate 'string out (format nil "~A~%" line)))))
      (is equal out (format nil "hello world!~%!dlrow olleh~%{hello world!}~%")))
    (setf (gethash "html" *output*) (list nil ".html"
					  (make-hash-table :test #'equal) "")))

(define-test append-output
    :parent primitives
    (append-output "html" "Hello" :string-buffer T)
    (append-output "html" "world!" :string-buffer T)
    (is equal (gethash "html" *string-buffer*) '("Hello" "world!"))
    (append-output "html" "Lorem" :string-buffer T)
    (append-output "html" "ipsum dolor sit" :string-buffer T)
    (append-output "html" "amet." :string-buffer T)
    (append-output "html" "END")
    (is equal (nth 3 (gethash "html" *output*))
	"Hello world! Lorem ipsum dolor sit amet. END")
    (reset-html-output))

(define-test tokenize-list
    :parent primitives
    (is equal (tokenize-list "(\"Lorem ipsum\" \\(dolor\\))")
	'("(" "Lorem ipsum" "(dolor)" ")")))

(define-test process-list
    :parent primitives
    (is equal (process-list
	       (tokenize-list "(hello (world dlrow (olleh) world))"))
	'(("hello" ("world" "dlrow" ("olleh") "world"))))
    (is equal (process-list (tokenize-list "(a b c (d e)) (f g (h i))"))
	'(("a" "b" "c" ("d" "e")) ("f" "g" ("h" "i")))))

(define-test load-list
    :parent primitives
    (is equal (load-list "tests/test-template")
	'(("a" ("b" "c" ("d" "e" "f"))))))
