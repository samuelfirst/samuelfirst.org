;;; ==========================================================================
;;; =============================== HTML Tests ===============================
;;; ==========================================================================

(define-test html-backend :parent all-tests)

;;; Test the html bold functionality
(define-test html-bold
    :parent html-backend
    (html-bold-pre)
    (html-bold "text")
    (html-bold-post)
    (is equal "<strong>text </strong>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html preformatted functionality
(define-test html-code
    :parent html-backend
    (html-code-pre)
    (html-code "First Line~%Second Line~%Third Line~%")
    (html-code-post)
    (is equal "<pre>First Line~%Second Line~%Third Line~%</pre>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html image functionality
(define-test html-image
    :parent html-backend
    (html-image-pre)
    (html-image "tests/image.png")
    (html-image-post)
    (is equal "<img src=\"tests/image.png\"/>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html link functionality
(define-test html-link
    :parent html-backend
    (html-link-pre "www.site.com")
    (html-link "Link Text")
    (html-link-post)
    (is equal "<a href=\"www.site.com\">Link Text</a>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html unordered list functionality
(define-test html-list
    :parent html-backend
    (html-list-pre)
    (html-list-item-pre)
    (html-list "item")
    (html-list-item-post)
    (html-list-post)
    (is equal "<ul><li>item </li></ul>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html header functionality
(define-test html-section
    :parent html-backend
    (loop for i in '("a" "b" "c" "d" "e" "f") doing
	  (html-section-pre)
	  (html-section i))
    (loop for i from 1 to 6 doing
	  (html-section-post))
    (is equal "<h1>a</h1><h2>b</h2><h3>c</h3><h4>d</h4><h5>e</h5><h6>f</h6>"
	(nth 3 (gethash "html" *output*)))
    (reset-html-output)
    (setf *html-section-depth* 1))

;;; Test to make sure the html header doesn't go past max depth (6)
(define-test html-section-max-depth
    :parent html-backend
    (setf *html-section-depth* 6)
    (html-section "a")
    (is = 6 *html-section-depth*)
    (reset-html-output)
    (setf *html-section-depth* 1))

;;; Test the html idiomatic functionality
(define-test html-slant
    :parent html-backend
    (html-slant-pre)
    (html-slant "a")
    (html-slant-post)
    (is equal "<i>a </i>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html table functionality
(define-test html-table
    :parent html-backend
    (html-table-pre)
    (html-table-row-pre)
    (html-table-cell-pre)
    (html-table-cell "a")
    (html-table-cell-post)
    (html-table-row-post)
    (html-table-post)
    (is equal "<table><tbody><tr><td>a </td></tr></tbody></table>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))

;;; Test the html paragraph functionality
(define-test html-text
    :parent html-backend
    (html-text-pre)
    (html-text "a")
    (html-text-post)
    (is equal "<p>a </p>" (nth 3 (gethash "html" *output*)))
    (reset-html-output))
