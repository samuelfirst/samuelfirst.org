(define-test macro :parent all-tests)

(define-test defwrapper-expansion
    :parent macro
    (is equal (macroexpand-1 '(defwrapper (hello-1 hello-2) (foo)
				(format t "hello ~A!" foo)))
	'(progn
	  (defun hello-1 (foo) (format t "hello ~A!" foo))
	  (defun hello-2 (foo) (format t "hello ~A!" foo)))))

(define-test string-case-expansion
    :parent macro
    (is equal (macroexpand-1 '(string-case "x"
			       ("x" (format t "x"))
			       ("y" (format t "y"))
			       (format t "z")))
	'(cond ((equal "x" "x") (format t "x"))
	       ((equal "y" "x") (format t "y"))
	       (T               (format t "z")))))
