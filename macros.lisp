;;; For clarity, it is desirable to define multiple functions that act as thin
;; wrappers and do the same thing.  For example, the `html-parse-text` and
;; `html-parse-bold` functions are identical, but for the sake of making parser
;; functions more readable, they are separate wrappers around the same output
;; function.  This results in a lot of code reuse, which bloats the code base.
;; The solution to both of these issues is to write a macro, allowing for a
;; single function body with multiple callers (well, technically separate
;; identical functions).
;; 
;; Usage:
;; ```
;; (defwrapper (hello-1 hello-2) (foo)
;;   (format t "hello ~A!" foo))
;;
;;    (hello-1 "world") => "hello world!"
;;    (hello-2 "world") => "hello world!"
;; ```
(defmacro defwrapper (func-names func-args &body body)
  `(progn
     ,@(loop for func in func-names collecting
	     `(defun ,func ,func-args ,@body))))

;;; Case statement syntactic equivalent for strings
;;
;; Usage:
;; ```
;; (let ((x "abc"))
;;   (string-case x
;;     ("cba" (format t "1")
;;     ("bca" (format t "2"))
;;     ("abc" (format t "3"))
;;     (format t "fall through")))) => "3"
;; ```
(defmacro string-case (var &body tests)
  `(cond
     ,@(loop for test in tests collecting
	     (if (stringp (car test))
		 `((equal ,(car test) ,var) ,@(cdr test))
		 `(T ,test)))))
