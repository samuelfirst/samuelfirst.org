.PHONY: doc
doc: doc/manual.tex
	cd doc; \
	pdflatex ./manual.tex; \
	pdflatex ./manual.tex; \
	rm  ./*.aux ./*.log ./*.out ./*.toc
