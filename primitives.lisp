;;;; All of the lower level functions that the higher level parsing functions
;;;; rely on.

;;; Boolean to control whether to run the main entry point
;; Roughly analagous to python's `if __name__ == __main__` construct.
(defvar *run-main* T)

;;; Hash table that holds lists of everything related to each output format
;; The lists are as follows: '(export file-extension template body), where
;; - export is a bool defining whether the format should be outputted
;; - file-extension is a string defining the extension to add when outputting
;; - template is the hash table of keys to substitute in the template
;; - body is the body of the content to output
(defvar *output* (make-hash-table :test #'equal))
(setf (gethash "html" *output*) (list nil ".html"
				      (make-hash-table :test #'equal) ""))
(setf (gethash "rss"  *output*) (list nil ".rssi"
				      (make-hash-table :test #'equal) ""))

;;; Hash table holding command line flags and their values
(defvar *args* (make-hash-table))
(setf (gethash :file *args*)    nil)
(setf (gethash :exclude *args*) nil)

;;; Parse command line arguments
(defun parse-args ()
  (let* ((args (cdr *posix-argv*))
	 (flag nil)
	 (i 0)
	 (last (- (length args) 1)))
    (loop for arg in args doing
	  (if (or (eq (char arg 0) #\-)
		  (and (= i last) (not (gethash :file *args*))))
	      (setf flag nil))
	  (cond ((not flag) (cond
			      ((member arg '("--help" "-h") :test #'equal)
			       (print-help) (quit))
			      ((member arg '("--exclude" "-e") :test #'equal)
			       (setf flag :exclude))
			      (t (setf (gethash :file *args*) arg))))
		(flag
		 (setf (gethash flag *args*)
		       (cons arg (gethash flag *args*)))))
	  (incf i))))

;;; Print help text to terminal
(defun print-help ()
  (format t
	  (concatenate
	   'string
	   "Usage: lispdown.lisp [OPT] <FILE>~%"
	   "Compile lispdown formatted files.~%~%"
	   "Arguments:~%"
	   "   -h, --help     display this help and exit~%"
	   "   -e, --exclude  export format(s) to exclude~%"
	   "~%"
	   "Examples:~%"
	   "   `lispdown lispdown-file`          Compile file to all formats~%"
	   "   `lispdown -e html lispdown-file`  Compile file and exclude html~%"
	   "~%"
	   "For further documentation on the lispdown spec, "
	   "see lispdown.md <https://gitlab.com/samuelfirst/"
	   "samuelfirst.org/-/blob/main/lispdown.md>~%")))

;;; Load a template file
;; Read in the template file and insert replacements.
(defun load-template (template inserts)
  (let ((loaded nil))
    (if (not (probe-file template))
	(error (format nil "File ~A does not exist~%" template)))
    (with-open-file (tmpl template)
      (do ((line (read-line tmpl) (read-line tmpl nil 'eof)))
	  ((eq line 'eof) nil)
	(let ((skip nil)
	      (in nil)
	      (insert ""))
	  (loop for c across line doing
		(let ((c (format nil "~A" c)))
		  (cond
		    (skip (setf skip nil)
			  (setf loaded (concatenate 'string loaded c)))
		    ((equal c "}")
		     (setf in nil)
		     (setf loaded
			   (concatenate 'string loaded
					(gethash insert inserts))))
		    (in (setf insert (concatenate 'string insert c)))
		    ((equal c "\\") (setf skip T))
		    ((equal c "{")  (setf in T))
		    (t (setf loaded (concatenate 'string loaded c))))))
	  (setf loaded (concatenate 'string loaded (format nil "~%"))))))
    (string-right-trim '(#\newline) loaded)))

;;; Process a tokenized list-string into a list
;; Loops over the tokens, recursively building the list structure
;; The state of the token list is kept consistent between recursions by using
;; multiple return values to pass the token list back to the caller function.
;; The input of: '("(" "a" "(" "b" "c" ")" "d" "e" ")" "(" "f" "(g)" ")")
;; Becomes: '(("a" ("b" "c") "d" "e") ("f "(g)"))
(defun process-list (lst)
  (let ((this-list '()))
    (flet ((append-list (x) (setf this-list (append this-list (list x))))
	   (consume () (setf lst (cdr lst))))
      (loop
       (if (not lst) (return))
       (let ((n (car lst)))
	 (consume)
	 (string-case n
		      ("(" (multiple-value-bind (tl l) (process-list lst)
			     (append-list tl)
			     (setf lst l)))
		      (")" (return))
		      (append-list n))))
      (values this-list lst))))

;;; Takes in a stringed list and tokenizes it for easier parsing.
;; Tokens are sequences of characters with the exception of open and close
;; parentheses separated by any number of white-space characters.  Open and
;; close parentheses are treated as their own tokens.  Backslash characters
;; escape the next character.
;; Meaning the string: "(a  (b c) d e) (f \(g\))"
;; is returned as: '("(" "a" "(" "b" "c" ")" "d" "e" ")" "(" "f" "(g)" ")")
(defun tokenize-list (str)
  (let ((ret '())
	(tok "")
	(skip nil)
	(in-string nil))
    (loop for c across str doing
	  (if skip (progn
		     (setf skip nil)
		     (setf tok (format nil "~a~a" tok c)))
	      (if (and in-string (not (equal c #\")))
		  (setf tok (format nil "~a~a" tok c))
	       (case c
		 (#\\ (setf skip T))
		 (#\" (setf in-string (not in-string)))
		 (#\( (if (not (equal tok ""))
			  (progn (setf ret (append ret (list tok)))
				 (setf tok "")))
		      (setf ret (append ret (list "("))))
		 (#\) (if (not (equal tok ""))
			  (progn (setf ret (append ret (list tok)))
				 (setf tok "")))
		      (setf ret (append ret (list ")"))))
		 ((#\Space #\Tab)
		  (if (not (equal tok ""))
		      (progn
			(setf ret
			      (append ret (list tok))) (setf tok ""))))
		 (T (setf tok (format nil "~a~a" tok c)))))))
    ret))

;;; Take in a file, read it into a string, and pass it to `tokenize-list` and
;; `process-list`
(defun load-list (path)
  (let ((content ""))
    (with-open-file (file path :direction :input :if-does-not-exist nil)
      (if (not file) (error (format nil "~A Does Not Exist!" path))
	  (progn
	    (loop as l = (read-line file nil) while l do
		  (setf content (concatenate 'string content l " "))))))
    (process-list (tokenize-list content))))

;;; Global variable that acts as a buffer for strings/syms to append to output
(defvar *string-buffer* (make-hash-table :test #'equal))
(loop for key being the hash-keys of *output* doing
      (setf (gethash key *string-buffer*) '()))

;;; Flush everything held in *string-buffer* for key to *output*
;; Format according to the following rules:
;; :: Spacing rules
;;   * If the element is not the first element, add a space before appending it
;;   * Append a space after the last element to ensure proper spacing between
;;     nested forms.
(defun flush-buffer-to-output (key)
  (let ((str "")
	(startp T))
    (loop for s in (gethash key *string-buffer*) doing
	  (if (not startp) (setf str (concatenate 'string str " ")))
	  (setf str (concatenate 'string str s))
	  (setf startp nil))
    (unless (equal str "") (setf str (concatenate 'string str " ")))
    (setf (nth 3 (gethash key *output*))
	  (concatenate 'string (nth 3 (gethash key *output*)) str)))
  (setf (gethash key *string-buffer*) '()))

;;; Append input to specified key in *output*
;; If :string-buffer is set to T, instead stick input in *string-buffer*
;; Otherwise, flush *string-buffer* and append input to output
(defun append-output (key input &key string-buffer)
  (cond (string-buffer
	 (setf (gethash key *string-buffer*)
	       (concatenate 'list (gethash key *string-buffer*) (list input))))
	(T
	 (flush-buffer-to-output key)
	 (setf (nth 3 (gethash key *output*))
	       (concatenate 'string (nth 3 (gethash key *output*)) input)))))

;;; Accepts a filename with an undesired extension and replaces the extension
(defun new-file-name (name ext)
  (concatenate 'string (subseq name 0 (search "." name :from-end t)) ext))

;;; Loop over each output format, assemble the file and write it out
(defun write-out-files ()
  (loop for format being the hash-keys of *output* doing
	(let* ((export        (nth 0 (gethash format *output*)))
	       (extension     (nth 1 (gethash format *output*)))
	       (template      (nth 2 (gethash format *output*)))
	       (body          (nth 3 (gethash format *output*)))
	       (template-path (gethash "path" template))
	       (out-file      (new-file-name (gethash :file *args*) extension)))
	  (if export
	      (progn
		(setf (gethash "body" template) body)
		(with-open-file (f out-file :direction :output
				            :if-exists :supersede)
		  (format f (load-template template-path template))))))))
