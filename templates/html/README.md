# HTML Templates
Lisp's use of s-expressions actually lends itsself really well to generating
html documents.  We can represent html (or any markup syntax, really) like
the following:

```
((html
	(head
		(title "Page Title")
		(link (= rel "stylesheet")
			  (= type "text/css")
			  (= href "<path>")))
	(body
		(div (= class "header")
			...)
		(div (= class "content")
			...)))
```

Which would then translate to:

```
<html>
	<head>
		<title>
			Page Title
		</title>
		<link rel="stylesheet" type="text/css" href="<path>">
		</link>
	</head>
	<body>
		<div class="header">
			...
		</div>
		<div class="content">
			...
		</div>
	</body>
</html>
``` 

# Differentiating Between Attributes and Inner Content
Consider the following:

```
(div
	(p "Para 1")
	(p "Para 2"))
	
(div
	(class "a")
	(p "Para"))
```

There is no easy way in the above to differentiate between an element that
should be an attribute or something that should be encased in the tag.  As I
see it, there are two solutions to this:

Adding more levels of cons cells

```
(div '()
	(p '() "Para 1")
	(p '() "Para 2"))
	
(div '((class "a"))
	(p '() "Para"))
```

or changing the cons cells to a list with a signifier at the beginning.

```
(div
	(p "Para 1")
	(p "Para 2"))
	
(div 
	(= class "a")
	(p "Para"))
```

The advantage of the first method is that parsing it will be moderately easier
and more in the metaphor of lisp.  The advantage of the second method is that the
templates will be both more readable and easier to write.  Because the parsing
code will only be written once and likely touched significantly less than the
template code, I believe that the second method is preferrable.

# Edgecases
There are a few irritating edgecases in html where it doesn't follow the '<open
tag, attribute=value>, inner content, <\/closing tag>' format.  Most of these
can probably be pretty safely ignored, since usually it will just result in
stray end tags, which browsers will just swallow.  Even the '<!DOCTYPE html>'
header can probably be safely ignored, although, it should be pretty easy to
just write an exception for the doctype definition.  It might be worth 
considering a similar solution to the above attribute vs. inner element problem.
