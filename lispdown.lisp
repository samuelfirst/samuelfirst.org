;;;; Library to generate web documents from templates
;;;; Documents to generate should include html, gemini, rss, etc.
;;;; Documents will be generated from both templates under the
;;;; templates directory and content files formatted in a lisp/markdown hybrid.
;;;; See lispdown.md for a writeup of the spec.

(load "./macros.lisp")
(load "./primitives.lisp")
(load "./ld-html.lisp")
(load "./ld-rss.lisp")

;;; Main entrypoint
;; * Parse command line arguments
;; * Load and parse lispdown file
;; * Write out file in various formats
(defun main ()
  (parse-args)
  (let ((ld-file (gethash :file *args*)))
    (if ld-file
	(progn
	  (parse (load-list ld-file))
	  (rss-set-body)
	  (write-out-files)
	  (if (nth 0 (gethash "rss" *output*))
	      (rss-post-compile)))
	(print-help))))

;;; Top level parser
;; From the top level, there are two data types that can be encountered: conses
;; and symbols.  If a cons cell is encountered, recurse with the car, then
;; recurse with the cdr.  If a symbol is encountered, call the relevant sub
;; level parser and pass it data's cdr, or, if it is not a valid keyword, throw
;; an error.
(defun parse (data)
  (let ((op (car data))
	(rest (cdr data)))
    (cond ((consp op) (parse op) (parse rest))
	  ((not op) nil) ; Capture NIL
	  ((stringp op) (string-case op
			  ("bold" (parse-bold rest))
			  ("code" (parse-code rest))
			  ("image" (parse-image rest))
			  ("link" (parse-link rest))
			  ("list" (parse-list rest))
			  ("section" (parse-section rest))
			  ("slant" (parse-slant rest))
			  ("table" (parse-table rest))
			  ("template" (parse-template rest))
			  ("text" (parse-text rest))
			  (error (format nil "Invalid Token: ~A~%" op))))
	  (t (error (format nil "Fallthrough on: ~A~%" op))))))

;;; The sub level parsers work by calling the relevant pre-parse hook for each
;;; output format, then evaluating the members of the passed list by type and
;;; handing them off to the relevant function.  Broadly, this should be: conses
;;; to the top level parse function and strings/symbols to the output builder
;;; functions.  After calling the relevant function, it should call the post-
;;; parse hooks for each output format.

;;; Parses bold token; expects either a string or symbol
;; * Run the pre-call hook(s)
;; * Loop over data, doing:
;;   * Type check; if cons, jump back to main, if sym/str, pass data to backends
;; * Run the post-call hook(s)
(defun parse-bold (data)
  (html-bold-pre)

  (loop for d in data doing
	(cond ((consp d) (parse d))
	      ((stringp d) (html-bold d))
	      (t (error (format nil "Invalid Bold Data Type: ~A~%" d)))))
  
  (html-bold-post))

;;; Parses code token; expects a filename (String)
;; * Run the pre-call hook(s)
;; * Open the file specified in the car of `data`
;; * Loop over each line of the file and pass them to the backends
;; * Run the post-call hook(s)
(defun parse-code (data)
  (html-code-pre)

  (let ((file (car data)))
    (if (stringp file)
	(progn
	  (with-open-file (code file
				:direction :input
				:if-does-not-exist nil)
	    (if (not code)
		(error (format nil "Unable to open: ~A" file))
		(progn
		  (loop as l = (read-line code nil)
			while l
			do (html-code (format nil "~A~%" l)))))))
	(error (format nil "Invalid Code Data Type: ~A~%" file))))

  (html-code-post))

;;; Parses the image token; expects a filename (String)
;; * Run the pre-call hook(s)
;; * Pass the car of `data` to the backends
;; * Run the post-call hook(s)
(defun parse-image (data)
  (html-image-pre)

  (let ((src (car data)))
    (if (stringp src)
	(progn
	  ;(if (not (probe-file src))
	  ;    (error (format nil "File ~A does not exist~%" data)))
	  (html-image src))
	(error (format nil "Invalid Image Data Type: ~A~%" src))))

  (html-image-post))

;;; Parses the link token; expects a URL (String) and link text (String)
;; * Pull out the URL and link text
;; * Run the pre-call hook(s) with the URL
;; * Pass the link text to the link backends
;; * Call the post hook(s)
(defun parse-link (data)
  (let ((link (cadr data))
	(text (car data)))

    (html-link-pre link)
    
    (html-link text)
   
    (html-link-post)))

;;; Parses the list token
;; * Run the pre-call hook(s)
;; * Loop over `data`, doing:
;;   * Call pre-item hook(s)
;;   * If datum is a cons cell, pass to top level parser
;;   * If datum is a symbol/string, pass to backends
;;   * Call post-item hook(s)
;; * Run the post-call hook(s)
(defun parse-list (data)
  (html-list-pre)

  (loop for d in data
      doing
      (html-list-item-pre)
      (cond ((consp d) (parse d))
	    ((stringp d) (html-list d))
	    (t (error (format nil "Invalid List Data Type: ~A~%" d))))
	(html-list-item-post))

  (html-list-post))

;;; Parses the secion token; expects section title (String)
;; * Run the pre-call hook(s)
;; * Pass the section title to the backend
;; * Run the post-call hook(s)
;; * Jump back to the main level of the parser
(defun parse-section (data)
  (flet ((consp-children (n) (let ((x T))
			       (loop for i in n doing
				     (setf x (and x (consp i)))) x)))
    (html-section-pre)

    (let ((title (car data))
	  (rest (cdr data)))
      (unless (and (stringp title) (consp-children rest))
	(error (format nil "Invalid Section Data Types")))
      (html-section title)
      (parse rest)
      (html-section-post))))

;;; Parses slant token; expects either a string or symbol
;; * Run the pre-call hook(s)
;; * Loop over data, doing:
;;   * Type check; if cons, jump back to main, if sym/str, pass data to backends
;; * Run the post-call hook(s)
(defun parse-slant (data)
  (html-slant-pre)

  (loop for d in data doing
	(cond ((consp d) (parse d))
	      ((stringp d) (html-slant d))
	      (t (error (format nil "Invalid Slant Data Type: ~A~%" d)))))
  
  (html-slant-post))

;;; Parses the table token; expects a list of conses (each representing a row)
;; * Run the table pre-call hook(s)
;; * Loop over data, doing:
;;   * Sanity check to make sure each row is a cons
;;   * Run the row pre-call hook(s)
;;   * Loop over row, doing:
;;     * Run the cell pre-call hook(s)
;;       * If cell data is a cons, jump to top-level parser
;;       * If cell data is a string or symbol, pass cell to html-table-cell
;;       * Otherwise, throw an error
;;     * Run the cell post-call hook(s)
;;   * Run the row post-call hook(s)
;; * Run the table post-call hook(s)
(defun parse-table (data)
  (html-table-pre)

  (loop for row in data doing
	(if (consp row)
	    (progn
	      (html-table-row-pre)
	      (loop for cell in row doing
		    (html-table-cell-pre)
		    (cond ((consp cell) (parse cell))
			  ((stringp cell) (html-table-cell cell))
			  (t (error (format nil
					    "Invalid Table Cell Data Type: ~A~%"
					    cell))))
		    (html-table-cell-post))
	      (html-table-row-post))
	    (error "Table may only contain CONS")))

  (html-table-post))

;;; Parses the template token; expects a file name
;; * Sanity check that type and path are strings
;; * Loop over keys doing:
;;   * Sanity check that keypair is a cons and key and value are both strings
;;   * Add each keypair to the hash table
;; * Set the entry in *templates* for export to true
;; * Set the entry in *templates* for type to the table
(defun parse-template (data)
  (let ((type (car data))
	(path (cadr data))
	(keys (cddr data))
	(table (make-hash-table :test #'equal)))
    (cond ((not (stringp type)) (error "Template type must be a String"))
	  ((not (stringp path)) (error "Template path must be a String")))
    (setf (gethash "path" table) path)
    (loop for k in keys doing
	  (cond ((not (consp k)) (error "Template keypairs must be conses"))
		((not (stringp (car k))) (error "Template keys must be strings"))
		((not (stringp (cadr k)))
		 (error "Template data must be strings")))
	  (setf (gethash (car k) table) (cadr k)))
    (setf (nth 0 (gethash type *output*)) T)
    (setf (nth 2 (gethash type *output*)) table)))

;;; Parses the text token; expects a series of strings or symbols
;; * Run the pre-call hook(s)
;; * Loop over the data doing:
;;   * Type check for string or symbol; then pass to backend or throw error
;; * Run the post-call hook(s)
(defun parse-text (data)
  (html-text-pre)

  (loop for d in data doing
	(cond ((consp d) (parse d))
	      ((stringp d) (html-text d))
	      (t (error (format nil "Invalid Text Data Type: ~A~%" d)))))
  
  (html-text-post))

;;; Call Main Entry point
(if *run-main*
    (main))
