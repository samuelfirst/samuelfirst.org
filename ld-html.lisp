;;;; Html Output Backend

;;; Define functions to append string/symbol to html output
(defwrapper (html-bold html-list html-slant
	     html-table-cell html-text) (token)
  (append-output "html" token :string-buffer T))

;;; Define functions to append only a string to html output
(defwrapper (html-code html-image html-link) (token)
  (append-output "html" token))

;;; === BOLD ===
(defun html-bold-pre  () (append-output "html" "<strong>"))
(defun html-bold-post () (append-output "html" "</strong>"))

;;; === CODE ===
;; HTML has two elements that can be used for code: <code> and <pre>.
;; The difference being that the code element crushes line breaks, so
;; for our purposes, the preformatted element is more appropriate.
(defun html-code-pre  () (append-output "html" "<pre>"))
(defun html-code-post () (append-output "html" "</pre>"))

;;; === IMAGE ===
(defun html-image-pre  () (append-output "html" "<img src=\""))
(defun html-image-post () (append-output "html" "\"/>"))

;;; === LINK ===
(defun html-link-pre (href)
  (append-output "html" (format nil "<a href=\"~A\">" href)))
(defun html-link-post () (append-output "html" "</a>"))

;;; === LIST ===
(defun html-list-pre       () (append-output "html" "<ul>"))
(defun html-list-item-pre  () (append-output "html" "<li>"))
(defun html-list-item-post () (append-output "html" "</li>"))
(defun html-list-post      () (append-output "html" "</ul>"))

;;; === SECTION ===
(defvar *html-section-depth* 1)

(defun html-section-pre ()
  (append-output "html" (format nil "<h~A>" *html-section-depth*)))

(defun html-section (title)
  (append-output "html" (format nil "~A</h~A>" title *html-section-depth*))
  (unless (eq *html-section-depth* 6)
    (incf *html-section-depth*)))

(defun html-section-post ()
  (decf *html-section-depth*))

;;; === SLANT ===
(defun html-slant-pre  () (append-output "html" "<i>"))
(defun html-slant-post () (append-output "html" "</i>"))

;;; === TABLE ===
(defun html-table-pre       () (append-output "html" "<table><tbody>"))
(defun html-table-row-pre   () (append-output "html" "<tr>"))
(defun html-table-cell-pre  () (append-output "html" "<td>"))
(defun html-table-cell-post () (append-output "html" "</td>"))
(defun html-table-row-post  () (append-output "html" "</tr>"))
(defun html-table-post      () (append-output "html" "</tbody></table>"))

;;; === TEXT ===
(defun html-text-pre  () (append-output "html" "<p>"))
(defun html-text-post () (append-output "html" "</p>"))
