\chapter{Language Specification}

\section{Syntax}

\subsection{Tokens}
Tokens are any sequence of characters separated by whitespace and not containing
reserved characters.  Reserved characters are opening and closing parentheses,
and whitespace characters (spaces and tabs).

\begin{table}[H]
  \caption{Token Examples}
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Token} & \textbf{Reasoning} \\
    \hline
    \verb|hello| & Valid: Doesn't contain reserved characters \\
    \verb|Good)bye| & Invalid: Contains a closing parenthesis \\
    \verb|ab cd| & Invalid: Contains a space; will be interpreted as two tokens \\
    \hline
  \end{tabular}
\end{table}

\subsection{Symbolic Expressions}
S-expressions, or sexps, are lists containing a series of tokens or other sexps.
Sexps are defined as any set of tokens between opening and closing parentheses.
The first token in an s-expression is evaluated as a keyword (unless otherwise
specified below).

These form the underlying structure of lispdown documents: nested sexps are used
to represent a hierarchical document.  For example, consider the following:

\begin{tcolorbox}
\begin{verbatim}
(section "Hello World!"
 (text Hello World))
\end{verbatim}
\end{tcolorbox}

In the above, the text form is known to be part of the 'Hello World!' section,
because it is contained within the section's expression.

\subsection{Escape Sequences}
Escape sequences allow for the use of reserved characters in symbols.  Lispdown
provides two methods of escaping characters: a backslash can be used to escape
a single character and any sequence of characters contained between double quotes
is treated as escaped.  Meaning that the following expressions:

\begin{tcolorbox}
\begin{verbatim}
(text Hello \(World!\))
(text Hello "(World!)")
\end{verbatim}
\end{tcolorbox}

Are both interpreted as an expression containing three tokens: 'text', 'Hello',
and '(World!)'.

\section{Keywords}

\subsection{Bold}
\begin{tcolorbox}
\begin{verbatim}
(bold <content>)

(bold This text will be emphasized)
\end{verbatim}
\end{tcolorbox}

The \verb|bold| keyword signifies that the contents should be emphasized through
being displayed in a thicker font face if possible, or by being bookended by
asterisks, if not.  It can contain any number of tokens or sexps.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output}  \\
    HTML & \verb|<strong>content</strong>| \\
  \end{tabular}
\end{table}

\subsection{Code}
\begin{tcolorbox}
\begin{verbatim}
(code <path>)

(code ./embed.py)
\end{verbatim}
\end{tcolorbox}

The \verb|code| keyword signifies an external file that should be displayed
verbatim and monospaced.  It may only contain a single token, specifying a valid
file location.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<pre>content</pre>| \\
  \end{tabular}
\end{table}

\subsection{Image}
\begin{tcolorbox}
\begin{verbatim}
(image <path>)

(image "pretty-picture.png")
\end{verbatim}
\end{tcolorbox}

The \verb|image| keyword signifies an external image that should be displayed
inline if possible; if not, the path to the file should be displayed instead.
It may only contain a single token, specifying a valid file location.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<img src="path"/>| \\
  \end{tabular}
\end{table}

\subsection{Link}
\begin{tcolorbox}
\begin{verbatim}
(link <text> <hyperref>)

(link foo https://www.samuelfirst.org)
(link "foo bar baz quux" mailto:me@site.net)
\end{verbatim}
\end{tcolorbox}

The \verb|link| keyword signifies a hyperlink.  It contains two tokens, the first
being the text to display for the link; the second being the link itself.  If the
output format does not support hyperlinks, the output should be:
\verb|<link text> (<hyperlink>)|.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<a href="hyperref">text</a>| \\
  \end{tabular}
\end{table}

\subsection{List}
\begin{tcolorbox}
\begin{verbatim}
(list <item> <item> <item>...)

(list
 (text Foo)
 (slant Bar)
 (bold Baz)
 (text Quux))
\end{verbatim}
\end{tcolorbox}

The \verb|list| form signifies an unordered (bulleted) list.  It can contain any
number of tokens or forms.  In output formats that do not support bulleted lists,
each token or form should be placed on a new line prepended by a dash.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<ul><li>item</li></ul>| \\
  \end{tabular}
\end{table}

\subsection{Section}
\begin{tcolorbox}
\begin{verbatim}
(section <title> ...)

(section "Hello World"
 (text Foo bar baz quux))

(section "Top Section"
 (section Subsection
  (text Foo)))
\end{verbatim}
\end{tcolorbox}

The \verb|section| form signifies a new section.  It contains a single token
defining the title of the section and any number of forms making up the section's
contents. A section within another section is treated as a subsection.  Maximum
subsection depth is defined by each backend; subsections exceeding the maximum
depth should be treated as the lowest available level of subsection (for example,
in html this would be h6).

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<hN>title</hN>...| \\
  \end{tabular}
\end{table}

\subsection{Slant}
\begin{tcolorbox}
\begin{verbatim}
(slant <text>)

(slant Foo bar baz quux)
\end{verbatim}
\end{tcolorbox}

The \verb|slant| form signifies italicized text.  It can contain any number of
tokens or forms.  If the output format does not support italics, the text should
be bookended by forward slashes.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<i>text</i>| \\
  \end{tabular}
\end{table}

\subsection{Table}
\begin{tcolorbox}
\begin{verbatim}
(table (...) (...) ...)

(table
 ((slant Foo) Bar)
 ((slant Baz) Quux))
\end{verbatim}
\end{tcolorbox}

The \verb|table| form signifies a table. It can contain any number of lists,
which can contain any number of forms or tokens.  If the output format does not
support tables, a table should be constructed in monospaced text with pipe
characters, dashes, and plus-signs forming the table's boundaries.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<table><tbody><tr><td>item</td></tr></tbody></table>| \\
  \end{tabular}
\end{table}

\subsection{Template}
\begin{tcolorbox}
\begin{verbatim}
(template <type> <path> (<key> <val>))

(template html templates/html/main
 (title "Foo Bar"))
\end{verbatim}
\end{tcolorbox}

The \verb|template| form does not represent any part of the document structure;
it tells the compiler which template to use for each output format and what keys
and values to pass to the template.  The \verb|template| form also controls
whether a format should be outputted: each template type specified will be
written out; any types unspecified will not be written out.  The template form
contains a symbol defining the type, followed by a symbol defining the path,
followed by any number of key-value pairs contained in parentheses.

\subsection{Text}
\begin{tcolorbox}
\begin{verbatim}
(text <text>)

(text Foo bar baz quux)
\end{verbatim}
\end{tcolorbox}

The \verb|text| form represents plain text.  It can contain any number of tokens
or forms.

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Format} & \textbf{Output} \\
    HTML & \verb|<p>text</p>| \\
  \end{tabular}
\end{table}
