\chapter{Overview}

\section{Control Flow}

\tikzset{block/.style={rectangle, very thick, draw=black}}
\tikzset{circle/.style={rectangle, rounded corners, very thick, draw=black}}

\begin{tikzpicture}
  \node (1) [block] {Arguments parsed};
  \node (2) [block, right=of 1] {File loaded and processed into list};
  \node (3) [block, below=of 2] {List passed to top-level parser};
  \node (4) [circle, below=of 3] {Parsing not done};
  \node (5) [block, below=of 4] {List passed to sublevel parser};
  \node (6) [circle, below=of 5] {List item is a token};
  \node (7) [block, below=of 6] {Token passed to backend};
  \node (8) [block, below=of 7] {Backend writes to appropriate output};
  \node (9) [circle, left=of 5] {List item is a sexp};
  \node (10) [circle, right=of 3] {Parsing done};
  \node (11) [block, below=of 10] {Write output to files};

  \path (1) edge[->] (2);
  \path (2) edge[->] (3);
  \path (3) edge[->] (4);
  \path (4) edge[->] (5);
  \path (5) edge[->] (6);
  \path (6) edge[->] (7);
  \path (7) edge[->] (8);
  \path (5) edge[->] (9);
  \path (9) edge[-] ($ (3.west) - (25mm,0) $);
  \path ($ (3.west) - (25mm,0) $) edge[->] (3.west);
  \path (3) edge[->] (10);
  \path (10) edge[->] (11);
\end{tikzpicture}

\section{Lisp Files}

\begin{table}[H]
  \begin{tabular}{|l|l|}
    \hline
    \textbf{File} & \textbf{Purpose} \\
    \hline
    lispdown.lisp   & Lispdown parser logic and main entrypoint \\
    macros.lisp     & Macro definitions \\
    primitives.lisp & Low level functions and data structures used by the rest of the codebase \\
    ld-html.lisp    & HTML backend \\
    ld-rss.lisp     & RSS backend \\
    \hline
  \end{tabular}
\end{table}

\subsection{Dependency Map}

\begin{tikzpicture}
  \node (macros) [circle] {macros.lisp};
  \node (primitives) [circle, right=of macros] {primitives.lisp};
  \node (lispdown) [circle, below left=of macros] {lispdown.lisp};
  \node (ldhtml) [circle, below right=of primitives] {ld-html.lisp};
  \node (ldrss) [circle, below=of ldhtml] {ld-rss.lisp};

  \path (macros) edge[->] (primitives);
  \path (macros) edge[->] (lispdown);
  \path (macros) edge[->] (ldhtml);
  \path (primitives) edge[->] (lispdown);
  \path (primitives) edge[->] (ldhtml);
  \path (primitives) edge[->] (ldrss);
  \path (ldhtml) edge[->] (lispdown);
  \path (ldrss) edge[->] (lispdown);
\end{tikzpicture}

The above dependency map shows the hierarchy of how files in the project rely on
eachother.  Arrows point from each file to the files that depend on it.  In
general: \verb|lispdown.lisp| will rely on every other file and backends will
always depend on \verb|primitives.lisp| and will usually depend on
\verb|macros.lisp|.

\section{General Program Structure}

The structure of the program can be best understood by breaking it up into
discreet parts: the underlying data structures, the various parsers, and the
backends for each output format.

\subsection{Data Structures}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|primitives.lisp| \\
  \end{tabular}
\end{table}

\subsubsection{Output Table}

The primary data structure is the output table stored in the variable
\verb|*output*|.  It is a hash table that expects a string key matching a valid
output format, where each entry holds a list containing: a boolean that describes
whether or not to export the format, a string containing the file extension for
the format, a hash table of key value pairs to pass to the template parser, and
a string that holds the body for each format.  Below is a representation of the
table:

\begin{table}[H]
  \begin{tabular}{|l|l|l|l|l|}
    \hline
    & \textbf{Nth 0} & \textbf{Nth 1} & \textbf{Nth 2} & \textbf{Nth 3} \\
    \hline
    \textbf{Key} & \textbf{Export Bool} & \textbf{Extension} & \textbf{Template Keys} & \textbf{Body} \\
    \hline
    html & NIL & .html & hash table & "" \\
    rss  & NIL & .rssi & hash table & "" \\
    \hline
  \end{tabular}
\end{table}

\subsubsection{Args Table}

The command line arguments are stored in the args table contained in the variable
\verb|*args*|.  The entries in the table correspond with the possible flags
that can be passed to the program, as well as an entry for the input file.  The
flag entries contain lists with all of the space-separated strings passed to the
flag.

\subsection{Command Line Argument Parser}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|primitives.lisp| \\
  \end{tabular}
\end{table}

The command line argument parser loops over the global \verb|*posix-argv*|
variable, and places each entry into the appropriate place in the args table.
The input file entry is defined as either the first entry not following a flag,
or the last entry in the list.  Whether an entry is a flag is determined by
checking whether it begins with a dash.  Any entries following that are not the
last entry or another flag are added to the appropriate flag in the args table.

\subsection{List Parser}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|primitives.lisp| \\
  \end{tabular}
\end{table}

The list parser consists of three parts: the list loader, the list tokenizer,
and the list processor.  The list loader reads in a file, collapses newlines
into spaces, passes that to the tokenizer and processor, and returns the
output.  The list tokenizer separates the list into tokens based on special
characters for easier processing.  The list processor takes the tokenized list
and recursively parses it into a nested list structure.

\subsection{Lispdown Parser}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|lispdown.lisp| \\
  \end{tabular}
\end{table}

The lispdown parser recursively processes a list passed to it, ceding control to
sublevel parsers when their cooresponding token is encountered.  The sublevel
parsers will then take the rest of the list following the token and process it,
passing it back to the top-level parser if a list is encountered, or to the
appropriate backend hooks if a token is encountered.

\subsection{Template Parser}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|primitives.lisp| \\
  \end{tabular}
\end{table}

The template parser takes in a template file and hash table of key-value pairs.
It reads in the template file and replaces each occurence of a string bookended
by curly brackets with the cooresponding value of that string in the hash table.
The final result is then returned as a string.

\subsection{Backends}
\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Location} & \verb|ld-*.lisp| \\
  \end{tabular}
\end{table}

Backends provide three hooks for each lispdown form, with the exception of
\verb|template|.  Pre-parse hooks are called before the form is processed,
content hooks are passed the processed content of the form, and post-parse hooks
are called after the form has been processed.  Each hook appends something to
the body entry of the appropriate key in the output table.  For example, in the
html backend (\verb|ld-html.lisp|), the pre-parse hook for the bold form appends
\verb|<strong>| to output, the content hook appends whatever string is passed to
it, and the post-parse hook appends \verb|</strong>|.
