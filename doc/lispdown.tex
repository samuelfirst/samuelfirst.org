\chapter{lispdown.lisp}

\section{Functions}

\subsection{main}
The \verb|main| function is the main entry point of the program.  It acts as a
thin glue layer to tie together the functions to parse cli arguments
(\verb|parse-args|), convert the file to a list (\verb|load-list|), parse the
list (\verb|parse|), and write out the output files (\verb|write-out-files|).

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(main)
\end{verbatim}
\end{tcolorbox}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
Template.html:
<html>
 <head>
  <title>{title}</title>
 </head>
 <body>
  {body}
 </body>
</html>
---
Lispdown.lispdown:
(template html path/to/Template.html
  (title "Foo Bar"))
(text Foo bar baz quux)
---
(main)
---
Lispdown.html:
<html>
 <head>
  <title>Foo Bar</title>
 </head>
 <body>
  <p>Foo bar baz quux</p>
 </body>
</html>
\end{verbatim}
\end{tcolorbox}

\subsection{parse}
The \verb|parse| function operates on a nested list structure.  It splits the
list into the car and cdr, then parses it according to the following rules:

\begin{itemize}
\item If the car is a cons, it parses the car, then parses the cdr
\item If the car is a string, it matches it with a sublevel parser and passes
  the cdr to the sublevel parser for processing
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse '((template html path/to/template
           (title "Foo Bar"))
         (text Foo bar baz quux)))
(nth 3 (gethash "html" *output*)) => <p>Foo bar baz quux</p>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-bold}
The \verb|parse-bold| function parses the contents of the bold form by:

\begin{itemize}
\item Calling the relevant pre-call hooks
\item Passing control back to the top-level parser if it encounters a cons
\item Passing control to backend content-hooks if it enounters a string
\item Calling the relevant post-call hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-bold list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-bold '(Foo bar))
(nth 3 (gethash "html" *output*)) => <strong>Foo bar</strong>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-code}
The \verb|parse-code| function handles the code form.  It expects a list with a
single string.  It then:

\begin{itemize}
\item Calls the relevant pre-parse hooks
\item Loops over the file, passing each line to the backends with a newline at
  the end
\item Calls the relevant post-parse hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-code list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
file.txt:
Foo Bar
Baz Quux
---
(parse-code '("file.txt")) => <pre>Foo Bar\nBaz Quux\n</pre>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-image}
The \verb|parse-image| function handles the image form.  It expects a list with
a single string containing a valid filename.  It then:

\begin{itemize}
\item Calls the relevant pre-parse hooks
\item Passes the string to the relevant backends
\item Calls the relevant post-parse hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-image list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-image '("image.png"))
(nth 3 (gethash "html" *output*)) => <img src="image.png"/>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-link}
The \verb|parse-link| function handles the link form.  It expects a list of two
strings and does the following:

\begin{itemize}
\item Calls the relevant pre-parse hooks
\item Passes data to the relevant backends
\item Calls the relevant post-parse hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-link list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-link '("foo" "www.bar.com"))
(nth 3 (gethash "html" *output*))
  => <a href="www.bar.com">foo</a>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-list}
The \verb|parse-list| function handles the list form as follows:

\begin{itemize}
\item Runs the relevant pre-parse-list hooks
\item Loops over the list running pre-item hooks, passing each item to the
  relevant backend, and running post-item hooks
\item Runs the relevant post-parse-list hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-list list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-list '("foo"))
(nth 3 (gethash "html" *output*)) => <ul><li>foo</li></ul>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-section}
The \verb|parse-section| function handles the section form.  It expects a list
with a string followed by a variable number of conses and does:

\begin{itemize}
\item Runs pre-call hooks with the passed string
\item Passes control back to the top-level parser for the rest of the items
\item Runs the post-call hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-section list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-section '("Foo" (text bar)))
(nth 3 (gethash "html" *output*)) => <h1>Foo</h1><p>bar</p>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-slant}
The \verb|parse-slant| function handles the slant form by:

\begin{itemize}
\item Runs the pre-call hooks
\item Loops over the passed list, passing strings to the relevant backend and
  conses back to the top-level parser
\item Runs the post-call hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-slant list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-slant '("foo"))
(nth 3 (gethash "html" *output*)) => <i>foo</i>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-table}
The \verb|parse-table| function handles the table form.  It expects a list of
lists and does the following:

\begin{itemize}
\item Runs the table pre-call hooks
\item Runs the row pre-call hooks for each row
\item Runs the cell pre-call hooks for each item in the row
\item Passes the contents of each item in the row to the backend
\item Runs the cell post-call hooks for each item in the row
\item Runs the row post-call hooks for each row
\item Runs the table post-call hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-table list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-table '((("foo"))))
(nth 3 (gethash "html" *output*))
  => <table><tbody><tr><td>foo</td></tr></tbody></table>
\end{verbatim}
\end{tcolorbox}

\subsection{parse-template}
The \verb|parse-template| function handles the template form.  It expects a list
with two strings followed by a variable number of conses and does the following:

\begin{itemize}
\item Uses the first string to determine the entry in the *output* table to use
\item Puts the second string in the path entry in the template table
\item Creates entries in the template table from the cons cells
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-template list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-template '("html" "path/to/template"
                  ("title" "foo")))
(gethash "path" (nth 2 (gethash "html" *output*))
  => path/to/template
(gethash "title" (nth 3 (gethash "html" *output*)))
  => foo
\end{verbatim}
\end{tcolorbox}

\subsection{parse-text}
The \verb|parse-text| function handles the text form by:

\begin{itemize}
\item Running the pre-call hooks
\item Passing each item to the relevant backend if it's a string or to the
  top-level parser if it's a cons
\item Running the post-call hooks
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-text list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & List to parse \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(parse-text '("foo"))
(nth 3 (gethash "html" *output*)) => <p>foo</p>
\end{verbatim}
\end{tcolorbox}
