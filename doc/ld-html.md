# ld-html

This is the html builder backend.

## Variables

### \*html-section-depth\*
Integer that stores the current (sub)section depth.  Should not go any higher
than 6.

## Functions

### append-html-output
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Sets output to the value of itsself appended with the passed string.  Output is
provided by the `html` entry in the `*output*` hashtable implemented by 
`primitives.lisp`.

### normalize-to-string
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | String        |

If the input is a string, it will return the string.  If the input is a symbol,
it will return the symbol as a lowercased string preceeded by a space.  For
example: 'FOO -> " foo".

### html-bold-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening emphasis tag (`<em>`) to output.

### html-bold
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | NIL           |

Appends normalized input to output.

### html-bold-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing emphasis tag (`</em>`) to output.

### html-code-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening preformatted tag (`<pre>`) to output.

### html-code
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Appends the input string to output.

### html-code-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing preformatted tag (`</pre>`) to output.

### html-image-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening part of the image tag (`<img src ="`) to output.

### html-image
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Appends the input string to output.

### html-image-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing part of the image tag (`"/>`) to output.

### html-link-pre
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Appends the opening part of the anchor tag with the string as the href, like
so: `<a href=[String]>`.

### html-link
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Appends the input string to output.

### html-link-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing anchor tag (`</a>`) to output.

### html-list-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening unordered list tag (`<ul>`) to output.

### html-list-item-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening list item tag (`<li>`) to output.

### html-list
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | NIL           |

Normalizes the input and appends it to the output.

### html-list-item-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing list item tag (`</li>`) to output.

### html-list-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing unordered list tag (`</ul>`) to output.

### html-section-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening section tag (`<hN>`) for the current level of section depth
(determined by `*html-section-depth*`).

### html-section
|         |        |
|---------|--------|
| expects | String |
| returns | NIL    |

Appends input string and closing section tag (`</hN>`) to output and increments
the level of section depth.

### html-section-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Decrements the current level of section depth.

### html-slant-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening idiomatic tag (`<i>`) to output.

### html-slant
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | NIL           |

Normalizes the input and appends it to output.

### html-slant-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing idiomatic tag (`</i>`) to output.

### html-table-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening table (`<table>`) and table body (`<tbody>`) tags to
output.

### html-table-row-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening table row (`<tr>`) tag to output.

### html-table-cell-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening table cell (`<td>`) tag to output.

### html-table-cell
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | NIL           |

Sanitizes the input and appends it to output.

### html-table-cell-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing table cell (`</td>`) tag to output.

### html-table-row-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing table row (`</tr>`) tag to output.

### html-table-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing table (`</table>`) and table body (`</tbody>`) tags to
output.

### html-text-pre
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the opening paragraph tag (`<p>`) to output.

### html-text
|         |               |
|---------|---------------|
| expects | String/Symbol |
| returns | NIL           |

Normalizes the input and appends it to output.

### html-text-post
|         |     |
|---------|-----|
| expects | NIL |
| returns | NIL |

Appends the closing paragraph tag (`</p>`) to output.
