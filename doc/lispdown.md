# Lispdown - Hybrid Lisp/Markdown Minilang
This approach has a couple of advantages over just using an existing markdown
syntax: lisps have very good list processing facilities (hence the name), which
will make writing parsers for a lisp-based minilang trivial; lisp dialects tend
to have fairly simple and non-sugary syntaxes, which will allow for the language
to be easily extended without creating laborious or confusing conventions (for
example, link syntax could look something like `(link ref)` instead of
`[link](ref)`).

## Syntax

Below is a simple example demonstrating the use of lispdown.  A more detailed
breakdown of each syntactic element can be found in the subsections beneath the
example.

```
(template "html" "path to template"
	      ("title" "Page Title"))
(section "Top Level Section"
	(text Some text, (link "a link" ref) , and some more text.)
	(section "This will be a subsection"
		 (image ref))
	(section "Yet another subsection"
		 (table
			 ("col 1" "col 2" "col 3")
			 ("dat 1" "dat 2" "dat 3")
			 ("dat 4" "dat 5" "dat 6")))
	(section "There can be up to 6 subsections"
		 (text There can be (bold bolded text) and (slant italicized) text.))
	(section "Lists are also available"
		 (list (text Bulleted lists)
			   (text only as)
			   (text of now)))
	(section "Inline code blocks would get a little wonky"
		 (text So we just pull from an external file)
    	 (code "Path to file")))
```

### Bold

```
(bold <content>)

(bold This text will be emphasized)
(bold (link This link will be emphasized))
```

Tokens contained in the `bold` form are treated as strings (with the exception of
other forms) and displayed as **bolded or emphasized**.  If a format does not 
have the ability to show bolded text, text should be displayed in ALL CAPS.

### Code

```
(code "<path>")

(code "embed.py")
```

The `code` form contains the path to a plain text file that should be displayed
verbatim and in a monospace font.

### Image

```
(image "<path>")

(image "pretty-picture.png")
```

The `image` form contains the path to an image file that should be displayed
inline.  If exporting to a format incapable of displaying an image, the path to
the image should be displayed instead.

### Link

```
(link "<text>" "<hyperref>")

(link "You are here." "https://www.samuelfirst.org")
(link "e-mail links work too!" "mailto:me@site.net")
```

The `link` form contains text as well as a hyperlink that the text should link
to.  If exporting to a format that does not support hyperlinks, the text should
be displayed followed by hyperreference in parentheses.

### List

```
(list (text/bold/slant <>) (text/bold/slant <>) ...)

(list (text First Item)
	  (text Second Item)
	  (text Third Item))
(list (text Normal)
	  (bold Bolded)
	  (slant))
```

The `list` form contains one or more `text`, `bold`, or `slant` forms.  It shold
be displayed as an unordered, or bulleted list.

### Section

```
(section "<text>" 
	...)

(section "h1"
	(section "h2"
		(section "h3"
			(section "h4"
				(section "h5"
					(section "h6"))))))
```

The `section` form denotes the beginning of a new section and contains the forms
of everything contained in that section.  If a `section` is nested inside of
another `section`, it becomes a subsection of that section.  There can be up to
six levels of subsections (aligning with h1-6 html tags).

### Slant

```
(slant <text>)

(slant This text is italicized)
```

The `slant` form contains text that should be displayed italicized.  If the 
export format does not support italicized text, the text should be bracketed by
double dashes (--text--).

### Table

```
(table 
	(...)
	(...)
	...)
	
(table
	("c1r1" "c2r1" "c3r1")
	("c1r2" "c2r2" "c3r2")
	("c1r3" "c2r3" "c3r3"))
```

The `table` form contains a series of conses representing rows, to be displayed
as tabular data.

### Template

```
(template "<type>" "<path>"
	      ("<key>" "<value>")
		  ...)

(template "html" "templates/html/default"
	      ("title" "My Page Title"))
(template "genesis" "templates/genesis/default"
	      ("title" "My Page Title"))
```

The `template` form contains the type of template and the path to the template
as the first two forms.  The next ~n~ forms should be conses containing a key
value pair cooresponding to keys defined in the specified template.

### Text
```
(text <tokens>)

(text Hello World!)
```

The `text` form contains a series of tokens that are interpreted as strings
unless they are other forms.
