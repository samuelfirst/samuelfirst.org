\chapter{primitives.lisp}

\section{Variables}

\subsection{*run-main*}
\begin{table}[H]
  \begin{tabular}{|l|l|}   
    \hline
    \textbf{Type}    & Boolean \\
    \textbf{Default} & T       \\
    \hline
  \end{tabular}
\end{table}

The \verb|*run-main*| variable describes whether the main entry point
\verb|(main)| should be run when executing \verb|lispdown.lisp|.  This is to
prevent the main function from being called in situations other than when the
program has been explicitly run (for example, it is not desirable to run the
main function when importing \verb|lispdown.lisp| for unit tests).  This variable
creates a structure roughly equivalent to python's \verb|if __name__ == __main__|
construct.  To disable the main function, you would do the following:

\begin{tcolorbox}
\begin{verbatim}
(load "path/to/primitives.lisp")
(setf *run-main* nil)
(load "path/to/lispdown.lisp")
\end{verbatim}
\end{tcolorbox}

\subsection{*output*}
\begin{table}[H]
  \begin{tabular}{|l|l|}   
    \hline
    \textbf{Type}    & Hash Table                      \\
    \textbf{Keys}    & "html", "rss"                   \\
    \textbf{Content} & (bool string hash-table string) \\
    \hline
  \end{tabular}
\end{table}

The \verb|*output*| variable holds the state for everything related to
transpiling and writing out the various output formats.  The keys are strings
corresponding to the name of each format.  Each key contains a list holding a
boolean describing whether the format should be written out, a string containing
the file extension to use when writing the file out, a hash table containing the
key/value pairs to pass to the template, and a string holding the compiled body
of the format.  See below for the default values for each list:

\begin{table}[H]
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Nth} & \textbf{Value} \\
    \hline
    0 & NIL        \\
    1 & ".ext"     \\
    2 & Hash table \\
    3 & ""         \\
    \hline
  \end{tabular}
\end{table}

\subsection{*args*}
\begin{table}[H]
  \begin{tabular}{|l|l|}   
    \hline
    \textbf{Type}    & Hash Table      \\
    \textbf{Keys}    & :file, :exclude \\
    \textbf{Content} & nil \\
    \hline
  \end{tabular}
\end{table}

The \verb|*args*| variable holds the parsed output of the command line arguments.
Its keys correspond to the flags that are interpreted by the \verb|parse-args|
function.  Each key contains nil until it is built into a list of the args
following the flag.

\subsection{*string-buffer*}
\begin{table}[H]
  \begin{tabular}{|l|l|}   
    \hline
    \textbf{Type}    & Hash Table            \\
    \textbf{Keys}    & Identical to *output* \\
    \textbf{Content} & Empty list            \\
    \hline
  \end{tabular}
\end{table}

The \verb|*string-buffer*| variable holds a series of lists of strings to be
appended to their assosciated body in \verb|*output*|.  It allows for the strings
to be written to \verb|*output*| with knowledge of context; meaning that spaces
can be added in the correct place.

\section{Functions}

\subsection{parse-args}
The \verb|parse-args| function parses the \verb|*posix-argv*| variable into the
\verb|*args*| table according to the following rules:

\begin{itemize}
\item Strings beginning with a dash are interpreted as flags
\item Strings following a flag belong to that flag
\item A string not following a flag is the interpreted as the input file
\item If the input file has not been specified, the last arg is the input file
\item The \verb|-h| and \verb|--help| flags are treated specially; they call
  the \verb|print-help| function and quit.
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(parse-args)
\end{verbatim}
\end{tcolorbox}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
*posix-argv* => ("lispdown.lisp" "--exclude" "rss"
                 "file.lispdown")
(parse-args)
(gethash :exclude *args*) => ("rss")
(gethash :file    *args*) => "file.lispdown"
\end{verbatim}
\end{tcolorbox}

\subsection{print-help}
The \verb|print-help| function prints text describing the usage of the program
to the terminal.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(print-help)
\end{verbatim}
\end{tcolorbox}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(print-help) => Usage: lispdown.lisp [OPT] <FILE>...
\end{verbatim}
\end{tcolorbox}

\subsection{load-template}
The \verb|load-template| function reads in a specified template file, replaces
key strings in the template with their assosciated value in the passed hash
table, and returns the resulting output.  The function also allows the use of a
backslash as an escape character.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(load-template "template-path" hash-table)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    template-path & String containing the path to the template file \\
    hash-table    & Hash table containing the key/value pairs \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
Template:
foo {foo} baz {baz}
---
(let ((ht (make-hash-table :test #'equal)))
  (setf (gethash "foo" ht) "bar")
  (setf (gethash "baz" ht) "quux")
  (load-template "path/to/template" ht)) => foo bar baz quux
\end{verbatim}
\end{tcolorbox}

\subsection{process-list}
The \verb|process-list| function takes in a tokenized list generated by
\verb|tokenize-list| and recursively converts it to a nested list of strings.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(process-list list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    list & tokenized list \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(let ((l (tokenize-list "(foo (bar (baz)) (quux))")))
  (process-list l)) => ("foo" ("bar" ("baz")) ("quux"))
\end{verbatim}
\end{tcolorbox}

\subsection{tokenize-list}
The \verb|tokenize-list| function takes in a stringed list and tokenizes it
according to the following rules:

\begin{itemize}
\item Tokens are any series of characters separated by spaces
\item Open and close parentheses are treated as their own tokens
\item Backslashes escape the next character
\item Bookending sequences with double quotes escapes the quoted sequence
\end{itemize}

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(tokenize-list stringed-list)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    stringed-list & String representation of a list \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(let ((l "(foo \\(bar\\) (baz quux))"))
  (tokenize-list l)) => ("(" "foo" "(bar)"
                         "(" "baz" "quux" ")" ")")
\end{verbatim}
\end{tcolorbox}

\subsection{load-list}
The \verb|load-list| function reads in a specified file, collapses the newlines
to spaces, tokenizes it with \verb|tokenize-list|, processes it with
\verb|process-list|, and returns the result.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(load-list path)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    path & String containing the path to the list file \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
List:
(foo (bar) (baz quux))
---
(load-list "path/to/list") => ("foo" ("bar") ("baz" "quux"))
\end{verbatim}
\end{tcolorbox}

\subsection{flush-buffer-to-output}
The \verb|flush-buffer-to-output| function flushes the strings stored in the
\verb|*string-buffer*| for the specified key and appends them to the body in
\verb|*output*| for the key.  It also adds spacing according to the following
rules:

\begin{itemize}
\item Any element other than the first element should have a space added before
  appending it
\item A space should be after all the strings have been appended
\end{itemize}

The buffer entry for the specified key is cleared after flushing it to output

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(flush-buffer-to-output key)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    key & String defining which entry in \verb|*string-buffer*| to use \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(gethash "html" *string-buffer*) => ("foo" "bar" "baz" "quux")
(nth 3 (gethash "html" *output*)) => ""

(flush-buffer-to-output "html")
(gethash "html" *string-buffer*) => nil
(nth 3 (gethash "html" *output*)) => "foo bar baz quux "
\end{verbatim}
\end{tcolorbox}

\subsection{append-output}
The \verb|append-output| function appends the input string to the specified key
in either \verb|*output*| or \verb|*string-buffer*|, determined by the value of
the string-buffer keyword argument.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(append-output key string :string-buffer bool)
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    key & String defining which entry in \verb|*string-buffer*| to use \\
    string & String to append \\
    bool & Boolean passed to the :string-buffer keyword argument \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(append-output "html" "foo" :string-buffer T)
(gethash "html" *string-buffer*) => ("foo")

(append-output "html" "bar")
(nth 3 (gethash "html" *output*)) => bar
\end{verbatim}
\end{tcolorbox}

\subsection{new-file-name}
The \verb|new-file-name| function takes in a filename with an extension, strips
the extension, and appends the new, specified extension to it.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(new-file-name "filename" "ext")
\end{verbatim}
\end{tcolorbox}

\begin{table}[H]
  \begin{tabular}{l l}
    \textbf{Arguments} & \textbf{Values} \\
    filename & String containing the filename \\
    ext & String containing the new extension \\
  \end{tabular}
\end{table}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(new-file-name "foo.bar" ".baz") => foo.baz
\end{verbatim}
\end{tcolorbox}

\subsection{write-out-files}
The \verb|write-out-files| function calls \verb|load-template| for each used
format and passes it the contents of the format's entry in the \verb|*output*|
table.  It then takes the returned value and writes it out to a file with the
new extension specified in its \verb|*output*| table entry.

\subsubsection{Usage}
\begin{tcolorbox}
\begin{verbatim}
(write-out-files)
\end{verbatim}
\end{tcolorbox}

\subsubsection{Example}
\begin{tcolorbox}
\begin{verbatim}
(gethash :filename *args*) => file.lispdown
(gethash "html" *output*) => (T ".html" <hash-table>
                              "<p>foo</p>")
(write-out-files)
---
file.html:
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <p>foo</p>
  </body>
</html>
\end{verbatim}
\end{tcolorbox}
