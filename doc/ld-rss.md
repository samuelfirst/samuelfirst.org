# ld-rss

## Variables

### \*n-items\*

Integer that holds the number of the most recent rssi files to include in the
output of `rss.xml`.

## Functions

### rss-post-compile

|         |     |
|---------|-----|
| expects | nil |
| returns | nil |

Hook that is meant to be run after the compilation and export steps.  Reads in
all `.rssi` files in the directory that the lispdown file is in, selects the n
most recent (specified by `*n-items*`), concatenates them, and uses them as the
body for the appropriate template.  The template to use is determined by the
content of the `"tpath"` key in the rss template.  The output file will always
be `rss.xml`.

### rss-set-body

|         |     |
|---------|-----|
| expects | nil |
| returns | nil |

Sets body of "rss" `*output*` key to the content of html's body.
