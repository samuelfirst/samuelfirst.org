;;;; Lispdown RSS builder
;;;; RSS feed generation presents a few problems that makes it a little tricky
;;;; to integrate smoothly with the existing structure of the lispdown parser.
;;;;  - Firstly: RSS feeds are consolidated to a single xml file with multiple
;;;;    items corresponding with links to separate html documents.  This doesn't
;;;;    mesh well with the existing templating system implementation that works
;;;;    on a per-file basis.
;;;;  - Secondly: RSS feeds exceeding 150kb might be ignored by some aggregators.
;;;;    This is the reason that a lot of rss feeds only include the 3-5 most
;;;;    recent items.
;;;; The solution to these problems that I believe is the smoothest integration
;;;; with the existing code base is as follows:
;;;;  - Files to be exported with an RSS feed should follow a regular format that
;;;;    allows them to be sorted by order of creation; ex: yyyy-mm-dd-filename
;;;;  - RSS feed content for each item should be generated and placed into
;;;;    <filename>.rssi.  This should include everything that would be placed
;;;;    into the item tag.
;;;;  - A separate template path for a top-level rss template should be passed
;;;;    to the parser with the "tpath" key:
;;;;      ```
;;;;      (template "rss" "<path>"
;;;;                ("tpath"  "<path to top-level rss template>")
;;;;                ...)
;;;;      ```
;;;;  - A post call hook should be run after the file output step in the main
;;;;    program loop that reads in the rssi files in the directory, selects the
;;;;    n most recent, concatenates them, runs them through the template parser,
;;;;    and writes them out to the file `rss.xml`

;;; Holds the number of items to include in the final output of the feed.
(defvar *n-items* 5)

;;; Hook run after files are compiled and written to disk
;; * Grab the template path from the rss template (key "tpath")
;; * Determine directory that the rssi file is in
;; * Read in the first *n-items* files in the directory and set body to their
;;   contents.
;; * Write out `rss.xml` in the directory containing the rssi file
;; NOTE: this functionality is subject to change; I think that it might be
;;       cleaner to separate this step out into a separate program.
(defun rss-post-compile ()
  (let* ((path (gethash "tpath" (nth 2 (gethash "rss" *output*))))
	 (template (make-hash-table :test #'equal))
	 (p (gethash :file *args*))
	 (dir (subseq p 0 (search "/" p :from-end T)))
	 (files (loop for f in
		      (directory (concatenate 'string dir "/*.rssi"))
		      collecting (format nil "~A" f)))
	 (sorted-files (sort files #'string>))
	 (files-to-use (funcall (lambda (n f) (if (plusp (- (length f) n))
						  (nbutlast f (- (length f) n))
						  f)) *n-items* sorted-files)))
    (setf (gethash "body" template) "")
    (loop for file in files-to-use doing
	  (with-open-file (f file)
	    (do ((line (read-line f) (read-line f nil 'eof)))
		((eq line 'eof) nil)
	      (setf (gethash "body" template)
		    (concatenate 'string
				 (gethash "body" template)
				 (format nil "~A~%" line))))))
    (with-open-file (f (concatenate 'string dir "/rss.xml")
		       :direction :output :if-exists :supersede)
      (format f (load-template path template)))))

;;; RSS summaries can (and should) be formatted as html.  Since we're already
;; generating html, it's easier to just copy it into the rss body after
;; compilation.  This would be easier if CL had a way to pass references.
(defun rss-set-body ()
  (setf (nth 3 (gethash "rss" *output*)) (nth 3 (gethash "html" *output*))))
